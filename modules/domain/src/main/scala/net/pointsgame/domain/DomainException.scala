package net.pointsgame.domain

final class DomainException(msg: String) extends Exception(msg)
