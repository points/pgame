package net.pointsgame.domain.repositories

import net.pointsgame.domain.model.Room

trait RoomRepository extends Repository[Room]
