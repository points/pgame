package net.pointsgame.domain.repositories

import net.pointsgame.domain.model.RoomMessage

trait RoomMessageRepository extends Repository[RoomMessage]
